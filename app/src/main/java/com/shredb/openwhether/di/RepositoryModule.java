package com.shredb.openwhether.di;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.shredb.openwhether.api.AddDefaultsInterceptor;
import com.shredb.openwhether.api.Api;
import com.shredb.openwhether.api.LiveDataCallAdapterFactory;
import com.shredb.openwhether.data.CityDao;
import com.shredb.openwhether.data.ForecastCallback;
import com.shredb.openwhether.data.ForecastDao;
import com.shredb.openwhether.data.ForecastDatabase;
import com.shredb.openwhether.data.ForecastRepository;
import com.shredb.openwhether.data.ForecastRepositoryFake;
import com.shredb.openwhether.data.ForecastRepositoryImpl;
import com.shredb.openwhether.util.AppExecutors;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.zip.GZIPInputStream;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static okhttp3.internal.Util.UTF_8;

@Module
abstract class RepositoryModule {

    @Singleton
    @Provides
    static ForecastDatabase provideDatabase(Application app, AppExecutors executors) {
        return Room.databaseBuilder(app, ForecastDatabase.class, "forecast.db")
                .addCallback(new ForecastCallback(app, executors))
                .build();
    }

    @Singleton
    @Provides
    static CityDao provideCityDao(ForecastDatabase db) {
        return db.cityDao();
    }

    @Singleton
    @Provides
    static ForecastDao provideForecastDao(ForecastDatabase db) {
        return db.forecastDao();
    }

    @Singleton
    @Provides
    static ForecastRepository provideForecastRepository(Api api, AppExecutors executors, CityDao cityDao, ForecastDao forecastDao) {
        return new ForecastRepositoryImpl(api, executors, cityDao, forecastDao);
    }
}
