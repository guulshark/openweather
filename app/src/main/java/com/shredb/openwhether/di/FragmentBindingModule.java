package com.shredb.openwhether.di;

import com.shredb.openwhether.ui.ForecastFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
abstract class FragmentBindingModule {
    @ContributesAndroidInjector
    abstract ForecastFragment contributeForecastFragment();
}
