package com.shredb.openwhether.di;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.shredb.openwhether.api.AddDefaultsInterceptor;
import com.shredb.openwhether.api.Api;
import com.shredb.openwhether.api.LiveDataCallAdapterFactory;
import com.shredb.openwhether.util.CityForecastTypeAdapterFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.zip.GZIPInputStream;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static okhttp3.internal.Util.UTF_8;

@Module
abstract class ApiModule {
    @Singleton
    @Provides
    static Retrofit provideRetrofit() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(new CityForecastTypeAdapterFactory())
                .create();
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(new AddDefaultsInterceptor())
                .addNetworkInterceptor(new HttpLoggingInterceptor()
                        .setLevel(HttpLoggingInterceptor.Level.BODY))
                .addNetworkInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(@NonNull Chain chain) throws IOException {
                        Response response = chain.proceed(chain.request());
                        if ("gzip".equals(response.header("Content-Encoding"))) {
                            ResponseBody body = response.body();
                            if (body != null) {
                                ByteArrayOutputStream baos = null;
                                try {
                                    int size;
                                    byte[] bytes = body.bytes();
                                    ByteArrayInputStream byteis = new ByteArrayInputStream(bytes);
                                    GZIPInputStream gzip = new GZIPInputStream(byteis);
                                    final int buffSize = 8192;
                                    byte[] tempBuffer = new byte[buffSize];
                                    baos = new ByteArrayOutputStream();
                                    while ((size = gzip.read(tempBuffer, 0, buffSize)) != -1) {
                                        baos.write(tempBuffer, 0, size);
                                    }
                                    MediaType contentType = body.contentType();
                                    Charset charset = contentType != null ? contentType.charset(UTF_8) : UTF_8;
                                    String respStr = new String(baos.toByteArray(), charset != null ? charset : UTF_8);
                                    response = response.newBuilder()
                                            .body(ResponseBody.create(contentType, bytes))
                                            .build();
                                    Log.d("Retrofit", "Decompressed response string - " + respStr);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } finally {
                                    if (baos != null) {
                                        baos.close();
                                    }
                                }
                            }
                        }
                        return response;
                    }
                });

        return new Retrofit.Builder()
                .client(builder.build())
                .baseUrl("http://api.openweathermap.org/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(LiveDataCallAdapterFactory.create())
                .build();
    }

    @Singleton
    @Provides
    static Api provideApi(Retrofit retrofit) {
        return retrofit.create(Api.class);
    }
}
