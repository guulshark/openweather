package com.shredb.openwhether.di;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.shredb.openwhether.util.AppExecutors;
import com.shredb.openwhether.util.DiskIOThreadExecutor;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module(includes = {
        ApiModule.class,
        RepositoryModule.class,
        ViewModelModule.class
})
abstract class AppModule {
    @Singleton
    @Provides
    static AppExecutors provideAppExecutors() {
        return new AppExecutors(new DiskIOThreadExecutor(),
                new AppExecutors.MainThreadExecutor());
    }

    @Singleton
    @Provides
    static SharedPreferences provideSharedPreferences(Application app) {
        return PreferenceManager.getDefaultSharedPreferences(app);
    }

    @Binds
    abstract Context bindContext(Application application);
}
