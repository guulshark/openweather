package com.shredb.openwhether.di;

import com.shredb.openwhether.ui.HomeActivity;
import com.shredb.openwhether.ui.SearchCityActivity;
import com.shredb.openwhether.ui.SelectCityActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
abstract class ActivityBindingModule {
    @ContributesAndroidInjector(modules = FragmentBindingModule.class)
    abstract HomeActivity contributeHomeActivity();

    @ContributesAndroidInjector
    abstract SelectCityActivity contributeSelectCityActivity();

    @ContributesAndroidInjector
    abstract SearchCityActivity contributeSearchCityActivity();
}
