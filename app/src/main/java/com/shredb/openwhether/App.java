package com.shredb.openwhether;

import android.graphics.Bitmap;

import com.shredb.openwhether.di.AppComponent;
import com.shredb.openwhether.di.DaggerAppComponent;
import com.squareup.picasso.Picasso;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

public class App extends DaggerApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        Picasso.Builder builder = new Picasso.Builder(this)
                .defaultBitmapConfig(Bitmap.Config.ARGB_8888);
        Picasso.setSingletonInstance(builder.build());
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponent appComponent = DaggerAppComponent.builder().application(this).build();
        appComponent.inject(this);
        return appComponent;
    }
}
