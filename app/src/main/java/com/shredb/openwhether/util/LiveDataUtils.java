package com.shredb.openwhether.util;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

/**
 * Created by macbook on 03.04.18.
 */

public class LiveDataUtils {
    private LiveDataUtils() {

    }

    @NonNull
    public static <T> LiveData<T> getDistinct(@NonNull LiveData<T> receiver) {
        final MediatorLiveData<T> distinctLiveData = new MediatorLiveData<>();
        distinctLiveData.addSource(receiver, new Observer<T>() {
            private boolean initialized;
            private T lastObj;

            @Override
            public void onChanged(@Nullable T obj) {
                if (!initialized) {
                    initialized = true;
                    lastObj = obj;
                    distinctLiveData.setValue(lastObj);
                } else if (obj == null && lastObj != null || (obj != null && !obj.equals(lastObj))) {
                    lastObj = obj;
                    distinctLiveData.setValue(lastObj);
                }
            }
        });
        return distinctLiveData;
    }

    @NonNull
    public static <T> LiveData<List<T>> getDistinctList(@NonNull LiveData<List<T>> receiver) {
        final MediatorLiveData<List<T>> distinctLiveData = new MediatorLiveData<>();
        distinctLiveData.addSource(receiver, new Observer<List<T>>() {
            private boolean initialized;
            private List<T> lastList;

            @Override
            public void onChanged(@Nullable List<T> list) {
                if (!initialized) {
                    initialized = true;
                    lastList = list;
                    distinctLiveData.setValue(lastList);
                } else if (list == null && lastList != null || (list != null && !list.equals(lastList))) {
                    lastList = list;
                    distinctLiveData.setValue(lastList);
                }
            }
        });
        return distinctLiveData;
    }
}
