package com.shredb.openwhether.util;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.shredb.openwhether.model.CityForecast;

public class CityForecastTypeAdapterFactory extends CustomizedTypeAdapterFactory<CityForecast> {

    public CityForecastTypeAdapterFactory() {
        super(CityForecast.class);
    }

    @Override
    void beforeWrite(CityForecast source, JsonElement toSerialize) {

    }

    @Override
    void afterRead(JsonElement deserialized) {
        JsonObject cityForecast = deserialized.getAsJsonObject();
        try {
            JsonObject sys = cityForecast.getAsJsonObject("sys");
            JsonPrimitive country = sys.getAsJsonPrimitive("country");
            cityForecast.add("country", country);
        } catch (Exception ignore) {
            ignore.printStackTrace();
        }
    }
}
