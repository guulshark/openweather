package com.shredb.openwhether.util;

import android.arch.lifecycle.LiveData;

/**
 * A LiveData class that has {@code null} value.
 */

public class NullLiveData extends LiveData {
    private NullLiveData() {
        //noinspection unchecked
        postValue(null);
    }
    public static <T> LiveData<T> create() {
        //noinspection unchecked
        return new NullLiveData();
    }
}
