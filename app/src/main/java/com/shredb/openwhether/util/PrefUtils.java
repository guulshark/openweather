package com.shredb.openwhether.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class PrefUtils {
    private static final String PREFIX = "pref_";

    /**
     * String preference storing ids of selected cities.
     */
    public static final String PREF_SELECTED_IDS = PREFIX + "selected_ids";


    public static SharedPreferences getPreferences(final Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void putSelectedIds(Context context, Set<Long> selectedIds) {
        getPreferences(context).edit()
                .putString(PREF_SELECTED_IDS, join("]", selectedIds, null))
                .apply();
    }

    @NonNull
    public static long[] getSelectedIds(Context context) {
        String[] strIds = getPreferences(context)
                .getString(PREF_SELECTED_IDS, "").split("]");
        long[] ids = new long[strIds.length];
        for (int i = 0; i < ids.length; i++) {
            try {
                ids[i] = Long.parseLong(strIds[i]);
            } catch (NumberFormatException e) {
                ids[i] = -1;
            }
        }
        return ids;
    }

    /**
     * Returns a string containing the tokens joined by delimiters.
     *
     * @param sb        pass the already created StringBuilder for optimization.
     *                  For example when the function is called in a loop.
     * @param delimiter the delimiter
     * @param tokens    a collection of objects to be joined. Strings will be formed from
     *                  the objects by calling object.toString().
     */
    public static String join(String delimiter, Collection<?> tokens, StringBuilder sb) {
        if (tokens == null || tokens.isEmpty()) {
            return "";
        }

        if (sb == null) {
            sb = new StringBuilder();
        } else {
            sb.delete(0, sb.length());
        }


        Iterator<?> it = tokens.iterator();
        if (it.hasNext()) {
            sb.append(it.next());
            while (it.hasNext()) {
                sb.append(delimiter);
                sb.append(it.next());
            }
        }
        return sb.toString();
    }
}