package com.shredb.openwhether.util;

import android.arch.lifecycle.LiveData;

/**
 * A LiveData class that has predefined value.
 */

public class ConstLiveData<T> extends LiveData {
    private ConstLiveData(T value) {
        //noinspection unchecked
        postValue(value);
    }
    public static <T> LiveData<T> create(T value) {
        //noinspection unchecked
        return new ConstLiveData(value);
    }
}
