package com.shredb.openwhether.data;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public abstract class ForecastDao extends BaseDao<Forecast> {
    @Query("SELECT * FROM forecast WHERE city_id = :cityId AND dt >= :dtFrom ORDER BY dt")
    public abstract LiveData<List<Forecast>> loadForecast(long cityId, long dtFrom);
}
