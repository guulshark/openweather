package com.shredb.openwhether.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;

import com.shredb.openwhether.model.Main;
import com.shredb.openwhether.model.Weather;

import java.util.List;

@Entity(tableName = "forecast",
        primaryKeys = {"city_id", "dt"},
        foreignKeys = @ForeignKey(entity = City.class, parentColumns = "id",
                childColumns = "city_id", onDelete = ForeignKey.CASCADE),
        indices = @Index("city_id"))
public class Forecast {
    @ColumnInfo(name = "city_id")
    public long cityId;
    public long dt;
    @Ignore
    public List<Weather> weather;
    @Embedded
    public Main main;
    @Embedded
    public Weather primaryWeather;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Forecast)) return false;

        Forecast forecast = (Forecast) o;

        if (cityId != forecast.cityId) return false;
        if (dt != forecast.dt) return false;
        if (main != null ? !main.equals(forecast.main) : forecast.main != null) return false;
        return primaryWeather != null ? primaryWeather.equals(forecast.primaryWeather) : forecast.primaryWeather == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (cityId ^ (cityId >>> 32));
        result = 31 * result + (int) (dt ^ (dt >>> 32));
        result = 31 * result + (main != null ? main.hashCode() : 0);
        result = 31 * result + (primaryWeather != null ? primaryWeather.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Forecast{" +
                "cityId=" + cityId +
                ", dt=" + dt +
                ", weather=" + weather +
                ", main=" + main +
                ", primaryWeather=" + primaryWeather +
                '}';
    }
}
