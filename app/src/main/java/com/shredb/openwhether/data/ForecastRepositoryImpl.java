package com.shredb.openwhether.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.SparseArray;

import com.shredb.openwhether.api.Api;
import com.shredb.openwhether.api.ApiBody;
import com.shredb.openwhether.model.CityForecast;
import com.shredb.openwhether.model.Resource;
import com.shredb.openwhether.model.Weather;
import com.shredb.openwhether.util.AppExecutors;
import com.shredb.openwhether.util.NullLiveData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Response;

@Singleton
public class ForecastRepositoryImpl implements ForecastRepository {
    private final static String TAG = "ForecastRepositoryImpl";
    private final CityDao mCityDao;
    private final ForecastDao mForecastDao;
    private final AppExecutors mAppExecutors;
    private final Api mApi;

    @Inject
    public ForecastRepositoryImpl(Api api, AppExecutors executors, CityDao cityDao, ForecastDao forecastDao) {
        mAppExecutors = executors;
        mForecastDao = forecastDao;
        mCityDao = cityDao;
        mApi = api;
    }

    public LiveData<List<City>> loadCities() {
        return mCityDao.loadCities();
    }

    @Override
    public LiveData<List<City>> loadSelectedCities(@NonNull long[] ids) {
        if (ids.length == 0) {
            return NullLiveData.create();
        } else {
            return mCityDao.loadSelectedCities(ids);
        }
    }

    public LiveData<Resource<List<CityForecast>>> findCity(String query) {
        if (query != null && query.length() > 0) {
            MediatorLiveData<Resource<List<CityForecast>>> liveCities = new MediatorLiveData<>();
            LiveData<Response<ApiBody<CityForecast>>> liveRemoteCities = mApi.findCity(query);
            liveCities.addSource(liveRemoteCities, getRemoteCitiesObserver(liveRemoteCities, liveCities));
            liveCities.setValue(Resource.loading((List<CityForecast>) null));
            return liveCities;
        } else {
            return NullLiveData.create();
        }
    }

    @Override
    public void addCities(final List<City> cities) {
        Runnable updateRunnable = new Runnable() {
            @Override
            public void run() {
                Log.d("addCities","forecasts = " + cities);
                int updCnt = mCityDao.update(cities);
                long[] ids = mCityDao.insert(cities);
                Log.d("addCities","updCnt = " + updCnt + ", ids = " + Arrays.toString(ids));
            }
        };
        mAppExecutors.diskIO().execute(updateRunnable);
    }

    public LiveData<List<Forecast>> loadForecasts(long cityId) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        LiveData<List<Forecast>> liveForecasts =
                mForecastDao.loadForecast(cityId, cal.getTimeInMillis());
        liveForecasts.observeForever(getCheckForecastsObserver(cityId, liveForecasts));
        return liveForecasts;
    }

    private void insertForecasts(@NonNull final List<Forecast> forecasts) {
        Runnable updateRunnable = new Runnable() {
            @Override
            public void run() {
                int updCnt = mForecastDao.update(forecasts);
                long[] ids = mForecastDao.insert(forecasts);
                Log.d("insertForecasts","updCnt = " + updCnt + ", ids = " + Arrays.toString(ids));
            }
        };
        mAppExecutors.diskIO().execute(updateRunnable);
    }

    private void loadRemoteForecasts(long cityId) {
        Log.d("CheckForecastsObserver","loadRemoteForecasts for cityId = " + cityId);
        LiveData<Response<ApiBody<Forecast>>> liveRemoteForecasts = mApi.getForecasts(cityId);
        liveRemoteForecasts.observeForever(getRemoteForecastsObserver(cityId, liveRemoteForecasts));
    }

    private Observer<Response<ApiBody<CityForecast>>> getRemoteCitiesObserver(final LiveData<Response<ApiBody<CityForecast>>> liveRemoteCities,
                                                                              final MediatorLiveData<Resource<List<CityForecast>>> liveCities) {
        return new Observer<Response<ApiBody<CityForecast>>>() {
            @Override
            public void onChanged(@Nullable Response<ApiBody<CityForecast>> response) {
                liveRemoteCities.removeObserver(this);
                liveCities.removeSource(liveRemoteCities);
                Log.d("RemoteCitiesObserver","response = " + response);
                if (response != null && response.isSuccessful()) {
                    ApiBody<CityForecast> apiBody = response.body();
                    List<CityForecast> result;
                    if (apiBody != null && apiBody.list != null) {
                        result = apiBody.list;
                        for (CityForecast cityForecast : result) {
                            cityForecast.dt *= 1000L;
                        }
                    } else {
                        result = Collections.emptyList();
                    }
                    liveCities.setValue(Resource.success(result));
                } else {
                    liveCities.setValue(Resource.error((List<CityForecast>) null));
                }
            }
        };
    }

    private Observer<Response<ApiBody<Forecast>>> getRemoteForecastsObserver(final long cityId,
                                                                             final LiveData<Response<ApiBody<Forecast>>> liveRemoteForecasts) {
        return new Observer<Response<ApiBody<Forecast>>>() {
            @Override
            public void onChanged(@Nullable Response<ApiBody<Forecast>> response) {
                liveRemoteForecasts.removeObserver(this);
                Log.d("RemoteForecastsObserver","response = " + response);
                if (response != null && response.isSuccessful()) {
                    ApiBody<Forecast> apiBody = response.body();
                    if (apiBody != null && apiBody.list != null) {
                        List<Forecast> forecasts = getDailyForecasts(cityId, apiBody.list);
                        insertForecasts(forecasts);
                        /*
                        for (Forecast forecast : apiBody.list) {
                            forecast.dt *= 1000L;
                            forecast.main.tempDay = forecast.main.tempNight = forecast.main.temp;
                            forecast.cityId = cityId;
                            if (forecast.weather != null && forecast.weather.size() > 0) {
                                forecast.primaryWeather = forecast.weather.get(0);
                            } else {
                                forecast.primaryWeather = Weather.EMPTY;
                            }
                        }
                        insertForecasts(apiBody.list);
                        */
                    }
                }
            }
        };
    }

    private Observer<List<Forecast>> getCheckForecastsObserver(final long cityId,
                                                               final LiveData<List<Forecast>> liveForecasts) {

        return new Observer<List<Forecast>>() {
            @Override
            public void onChanged(@Nullable List<Forecast> forecasts) {
                liveForecasts.removeObserver(this);
                boolean loadRemote = forecasts == null || forecasts.isEmpty();
                if (forecasts != null && !forecasts.isEmpty()) {
                    long maxDt = 0;
                    for (Forecast forecast : forecasts) {
                        if (forecast.dt > maxDt) maxDt = forecast.dt;
                    }
                    // 105 hours = 4 days and
                    loadRemote = maxDt - System.currentTimeMillis() < TimeUnit.HOURS.toMillis(105) / 1000L;
                    Log.d("CheckForecastsObserver","maxDt = " + maxDt
                            + ", loadRemote = " + loadRemote);
                } else {
                    Log.d("CheckForecastsObserver","empty");
                }
                if (loadRemote) {
                    loadRemoteForecasts(cityId);
                }
            }
        };
    }

    /**
     * This method calculates average day and night temperature. Day from 9 till 21,
     * night from 21 till 9. But these calculations should take into account the time zone
     * of the city, which is unknown to us. So all calculations are based on the UTC time zone.
     * @param forecasts
     * @return
     */
    private List<Forecast> getDailyForecasts(long cityId, List<Forecast> forecasts) {
        // All data before 9 should refer to the previous day, so set GMT-9 time zone
        SparseArray<List<Forecast>> dailyListForecasts = new SparseArray<>();
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT-9"));
        for (Forecast forecast : forecasts) {
            forecast.dt *= 1000L;
            cal.setTimeInMillis(forecast.dt);
            int day = cal.get(Calendar.DAY_OF_YEAR);
            List<Forecast> list = dailyListForecasts.get(day);
            if (list == null) {
                list = new ArrayList<>();
                dailyListForecasts.put(day, list);
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                forecast.dt = cal.getTimeInMillis();
            }
            list.add(forecast);
        }

        List<Forecast> dailyForecasts = new ArrayList<>(dailyListForecasts.size());
        for (int i = 0; i < dailyListForecasts.size(); i++) {
            float tempDay = 0; // day from 9 till 21
            float tempNight = 0; // night from 21 till 9
            int dayCount = 0, nightCount = 0;
            cal.setTimeZone(TimeZone.getTimeZone("UTC"));
            List<Forecast> forecastList = dailyListForecasts.valueAt(i);
            Map<Weather, Integer> weatherCounter = new HashMap<>();
            for (Forecast forecast : forecastList) {
                cal.setTimeInMillis(forecast.dt);
                int hour = cal.get(Calendar.HOUR_OF_DAY);
                if (hour >= 9 && hour <= 21) {
                    dayCount++;
                    tempDay += forecast.main.temp;
                }
                if (hour <= 9 || hour >= 21) {
                    nightCount++;
                    tempNight += forecast.main.temp;
                }
                if (forecast.weather != null && forecast.weather.size() > 0) {
                    Weather weather = forecast.weather.get(0);
                    if (weather != null) {
                        Integer count = weatherCounter.get(weather);
                        if (count == null) {
                            weatherCounter.put(weather, 1);
                        } else {
                            weatherCounter.put(weather, ++count);
                        }
                    }
                }
            }
            Map.Entry<Weather, Integer> primaryEntry = null;
            for (Map.Entry<Weather, Integer> entry : weatherCounter.entrySet()) {
                if (primaryEntry == null || primaryEntry.getValue() < entry.getValue()) {
                    primaryEntry = entry;
                }
            }
            Forecast forecast = forecastList.get(0);
            forecast.cityId = cityId;
            forecast.primaryWeather = primaryEntry != null ? primaryEntry.getKey() : Weather.EMPTY;
            forecast.main.tempDay = dayCount > 0 ? tempDay / dayCount : Float.MIN_VALUE;
            forecast.main.tempNight = nightCount > 0 ? tempNight / nightCount : Float.MIN_VALUE;
            dailyForecasts.add(forecast);
        }
        Log.d("getDailyForecasts","dailyForecasts = " + dailyForecasts);
        return dailyForecasts;
    }
}
