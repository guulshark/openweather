package com.shredb.openwhether.data;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import java.util.List;

@Dao
public abstract class CityDao extends BaseDao<City> {
    @Query("SELECT * FROM city ORDER BY name")
    public abstract LiveData<List<City>> loadCities();

    @Query("SELECT * FROM city WHERE id IN (:ids) ORDER BY name")
    public abstract LiveData<List<City>> loadSelectedCities(@NonNull long[] ids);
}
