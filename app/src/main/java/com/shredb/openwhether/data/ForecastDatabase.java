package com.shredb.openwhether.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {City.class, Forecast.class}, version = 1)
public abstract class ForecastDatabase extends RoomDatabase {
    public abstract CityDao cityDao();
    public abstract ForecastDao forecastDao();
}
