package com.shredb.openwhether.data;

import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Update;

import java.util.List;

public abstract class BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract long insert(T entity);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract long[] insert(List<T> entities);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    public abstract int update(T entity);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    public abstract int update(List<T> entities);

    @Delete
    public abstract int delete(List<T> entities);
}
