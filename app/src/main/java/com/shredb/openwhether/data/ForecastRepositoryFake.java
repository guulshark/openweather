package com.shredb.openwhether.data;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shredb.openwhether.model.CityForecast;
import com.shredb.openwhether.model.Location;
import com.shredb.openwhether.model.Resource;
import com.shredb.openwhether.util.ConstLiveData;
import com.shredb.openwhether.util.NullLiveData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ForecastRepositoryFake implements ForecastRepository {
    private final static String TAG = "ForecastRepositoryImpl";

    private final List<City> NEW_CITIES;
    private final List<City> CITIES;
    private final List<Forecast> FORECASTS;

    @Inject
    public ForecastRepositoryFake() {
        NEW_CITIES = new ArrayList<>();
        CITIES = new ArrayList<>();
        FORECASTS = new ArrayList<>();
        setupFakeValues();
    }

    public LiveData<List<City>> loadCities() {
        return ConstLiveData.create(CITIES);
    }

    public LiveData<List<City>> loadSelectedCities(@NonNull long[] ids) {
        if (ids.length == 0) {
            return NullLiveData.create();
        } else {
            Arrays.sort(ids);
            List<City> selected = new ArrayList<>();
            for (City city : CITIES) {
                if (Arrays.binarySearch(ids, city.id) >= 0) {
                    selected.add(city);
                }
            }
            return ConstLiveData.create(selected);
        }
    }

    @Override
    public LiveData<Resource<List<CityForecast>>> findCity(String query) {
        return NullLiveData.create();
    }

    @Override
    public void addCities(List<City> cities) {
        //do nothing
    }
//    public LiveData<List<CityForecast>> findCity(String query) {
//        if (query != null) {
//            List<City> cities = new ArrayList<>();
//            for (City city : NEW_CITIES) {
//                if (city.name.contains(query)) {
//                    cities.add(city);
//                }
//            }
//            return ConstLiveData.create(cities);
//        }
//        return NullLiveData.create();
//    }

    public LiveData<List<Forecast>> loadForecasts(long cityId) {
        return ConstLiveData.create(FORECASTS);
    }

    private void setupFakeValues() {
        // found cities
        City city = new City();
        city.id = 519188;
        city.name = "Novinki";
        city.country = "RU";
        city.coord = new Location(55.683334, 37.666668);
        NEW_CITIES.add(city);
        city = new City();
        city.id = 1283240;
        city.name = "Kathmandu";
        city.country = "NP";
        city.coord = new Location(27.716667, 85.316666);
        NEW_CITIES.add(city);

        // cities
        city = new City();
        city.id = 524901;
        city.name = "Moscow";
        city.country = "RU";
        city.coord = new Location(55.75222, 37.615555);
        CITIES.add(city);
        city = new City();
        city.id = 498817;
        city.name = "Saint Petersburg";
        city.country = "RU";
        city.coord = new Location(59.894444, 30.264168);
        CITIES.add(city);
        city = new City();
        city.id = 2643743;
        city.name = "London";
        city.country = "GB";
        city.coord = new Location(51.50853, -0.12574);
        CITIES.add(city);
        city = new City();
        city.id = 1850147;
        city.name = "Tokyo";
        city.country = "JP";
        city.coord = new Location(35.689499, 139.691711);
        CITIES.add(city);

        String json = "[{\"dt\":1522789200,\"main\":{\"temp\":-1.76,\"temp_min\":-2.72," +
                "\"temp_max\":-1.76,\"pressure\":1009.67,\"sea_level\":1030.4,\"grnd_level\":1009.67," +
                "\"humidity\":90,\"temp_kf\":0.96},\"weather\":[{\"id\":800,\"main\":\"Clear\"," +
                "\"description\":\"ясно\",\"icon\":\"01n\"}],\"clouds\":{\"all\":0},\"wind\":" +
                "{\"speed\":2.76,\"deg\":252.001},\"rain\":{},\"snow\":{},\"sys\":{\"pod\":\"n\"}," +
                "\"dt_txt\":\"2018-04-03 21:00:00\"}," +
                "{\"dt\":1522800000,\"main\":{\"temp\":-4,\"temp_min\":-4.64," +
                "\"temp_max\":-4,\"pressure\":1011.01,\"sea_level\":1031.86,\"grnd_level\":1011.01," +
                "\"humidity\":86,\"temp_kf\":0.64},\"weather\":[{\"id\":800,\"main\":\"Clear\"," +
                "\"description\":\"ясно\",\"icon\":\"02n\"}],\"clouds\":{\"all\":8},\"wind\":" +
                "{\"speed\":2.82,\"deg\":250.509},\"rain\":{},\"snow\":{},\"sys\":{\"pod\":\"n\"}," +
                "\"dt_txt\":\"2018-04-04 00:00:00\"}," +
                "{\"dt\":1522810800,\"main\":{\"temp\":-6.34,\"temp_min\":-6.66," +
                "\"temp_max\":-6.34,\"pressure\":1011.92,\"sea_level\":1032.74,\"grnd_level\":1011.92," +
                "\"humidity\":83,\"temp_kf\":0.32},\"weather\":[{\"id\":802,\"main\":\"Clouds\"," +
                "\"description\":\"слегка облачно\",\"icon\":\"03d\"}],\"clouds\":{\"all\":32},\"wind\":" +
                "{\"speed\":1.21,\"deg\":228.001},\"rain\":{},\"snow\":{},\"sys\":{\"pod\":\"d\"}," +
                "\"dt_txt\":\"2018-04-04 03:00:00\"}," +
                "{\"dt\":1522821600,\"main\":{\"temp\":-0.82,\"temp_min\":-0.82," +
                "\"temp_max\":-0.82,\"pressure\":1013.01,\"sea_level\":1033.66,\"grnd_level\":1013.01," +
                "\"humidity\":93,\"temp_kf\":0},\"weather\":[{\"id\":800,\"main\":\"Clear\"," +
                "\"description\":\"ясно\",\"icon\":\"01d\"}],\"clouds\":{\"all\":76},\"wind\":" +
                "{\"speed\":1.72,\"deg\":222.509},\"rain\":{},\"snow\":{\"3h\":0.0085},\"sys\":{\"pod\":\"d\"}," +
                "\"dt_txt\":\"2018-04-04 06:00:00\"}]";
        List<Forecast> forecasts = new Gson().fromJson(json, new TypeToken<List<Forecast>>() {}.getType());
        for (Forecast forecast : forecasts) {
            forecast.main.tempNight = forecast.main.temp - 3;
            forecast.main.tempDay = forecast.main.temp + 3;
            if (forecast.weather != null && forecast.weather.size() > 0) {
                forecast.primaryWeather = forecast.weather.get(0);
            }
        }
        FORECASTS.addAll(forecasts);
    }
}
