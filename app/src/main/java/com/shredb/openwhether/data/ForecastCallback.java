package com.shredb.openwhether.data;

import android.app.Application;
import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.RoomDatabase;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shredb.openwhether.util.AppExecutors;
import com.shredb.openwhether.util.PrefUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by macbook on 03.04.18.
 */

public class ForecastCallback extends RoomDatabase.Callback {
    private final Application mApplication;
    private final AppExecutors mAppExecutors;

    public ForecastCallback(Application app, AppExecutors executors) {
        mApplication = app;
        mAppExecutors = executors;
    }

    @Override
    public void onCreate(@NonNull final SupportSQLiteDatabase db) {
        mAppExecutors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                InputStream inputStream = null;
                List<City> cities = null;
                try {
                    inputStream = mApplication.getAssets().open("city_list.json");
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    cities = new Gson().fromJson(reader, new TypeToken<List<City>>() {}.getType());
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException ignore) {
                            ignore.printStackTrace();
                        }
                    }
                }

                Log.d("ForecastCallback", "onCreate: cities=" + cities);
                if (cities != null) {
                    db.beginTransaction();
                    try {
                        for (City city : cities) {
                            ContentValues cv = new ContentValues();
                            cv.put("id", city.id);
                            cv.put("name", city.name);
                            cv.put("country", city.country);
                            cv.put("lat", city.coord.lat);
                            cv.put("lon", city.coord.lon);
                            db.insert("city", SQLiteDatabase.CONFLICT_IGNORE, cv);
                        }
                        db.setTransactionSuccessful();
                        Set<Long> ids = new HashSet<>();
                        ids.add(498817L);
                        PrefUtils.putSelectedIds(mApplication, ids);
                    } catch (SQLiteException e) {
                        e.printStackTrace();
                    } finally {
                        db.endTransaction();
                    }
                }
            }
        });
    }
}
