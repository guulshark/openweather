package com.shredb.openwhether.data;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.shredb.openwhether.model.CityForecast;
import com.shredb.openwhether.model.Resource;

import java.util.Collection;
import java.util.List;

public interface ForecastRepository {
    LiveData<List<City>> loadCities();
    LiveData<List<City>> loadSelectedCities(@NonNull long[] ids);
    LiveData<Resource<List<CityForecast>>> findCity(String query);
    LiveData<List<Forecast>> loadForecasts(long cityId);
    void addCities(List<City> cities);
}
