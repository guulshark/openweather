package com.shredb.openwhether.api;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LiveDataCallAdapterFactory extends CallAdapter.Factory {

    public static LiveDataCallAdapterFactory create() {
        return new LiveDataCallAdapterFactory();
    }

    private LiveDataCallAdapterFactory() {

    }

    @Nullable
    @Override
    public CallAdapter<?, ?> get(@NonNull Type returnType, @NonNull Annotation[] annotations, @NonNull Retrofit retrofit) {
        if (getRawType(returnType) != LiveData.class) {
            return null;
        }
        if (!(returnType instanceof ParameterizedType)) {
            throw new IllegalArgumentException("LiveData return type must be parameterized " +
                    "as LiveData<Foo> or CompletableFuture<? extends Foo>");
        }
        Type innerType = getParameterUpperBound(0, (ParameterizedType) returnType);
        if (getRawType(innerType) == Response.class) {
            // Generic type is Response<T>. Extract T and create the Response version of the adapter.
            if (!(innerType instanceof ParameterizedType)) {
                throw new IllegalStateException("Response must be parameterized"
                        + " as Response<Foo> or Response<? extends Foo>");
            }
            Type responseType = getParameterUpperBound(0, (ParameterizedType) innerType);
            return new ResponseCallAdapter<>(responseType);
        }
        /*
        else if (getRawType(innerType) == ApiResponse.class) {
            // Generic type is ApiResponse<T>. Extract T and create the ApiResponse version of the adapter.
            if (!(innerType instanceof ParameterizedType)) {
                throw new IllegalStateException("ApiResponse must be parameterized"
                        + " as ApiResponse<? extends ApiBody>");
            }
            Type apiResponseType = getParameterUpperBound(0, (ParameterizedType) innerType);
            return new ApiResponseCallAdapter<>(apiResponseType);
        }
        */
        // Generic type is not Response<T> nor ApiResponse<T>. Use it for body-only adapter.
        return new BodyCallAdapter<>(innerType);
    }

    /*
    private static final class ResponseCallAdapter<R>
            implements CallAdapter<R, LiveData<Response<R>>> {
        private final Type responseType;

        ResponseCallAdapter(Type responseType) {
            this.responseType = responseType;
        }

        @Override
        public Type responseType() {
            return responseType;
        }

        @Override
        public LiveData<Response<R>> adapt(@NonNull final Call<R> call) {
            return new LiveData<Response<R>>() {
                private AtomicBoolean mComputing = new AtomicBoolean(false);

                @Override
                protected void onActive() {
                    if (mComputing.compareAndSet(false, true)) {
                        call.enqueue(new Callback<R>() {
                            @Override
                            public void onResponse(@NonNull Call<R> call, @NonNull Response<R> response) {
                                postValue(response);
                            }

                            @SuppressWarnings("unchecked")
                            @Override
                            public void onFailure(@NonNull Call<R> call, @NonNull Throwable throwable) {
                                postValue((Response<R>) Response.error(500,
                                        ResponseBody.create(MediaType.parse("text/plain"),
                                                throwable.getMessage() != null ?
                                                        throwable.getMessage() :
                                                        throwable.toString())));
                            }
                        });
                    }
                }
            };
        }
    }

    private static final class ApiResponseCallAdapter<R extends ApiBody>
            implements CallAdapter<R, LiveData<ApiResponse<R>>> {
        private final Type responseType;

        ApiResponseCallAdapter(Type responseType) {
            this.responseType = responseType;
        }

        @Override
        public Type responseType() {
            return responseType;
        }

        @Override
        public LiveData<ApiResponse<R>> adapt(@NonNull final Call<R> call) {
            return new LiveData<ApiResponse<R>>() {
                private AtomicBoolean mComputing = new AtomicBoolean(false);

                @Override
                protected void onActive() {
                    if (mComputing.compareAndSet(false, true)) {
                        call.enqueue(new Callback<R>() {
                            @Override
                            public void onResponse(@NonNull Call<R> call, @NonNull Response<R> response) {
                                postValue(new ApiResponse<>(response));
                            }

                            @Override
                            public void onFailure(@NonNull Call<R> call, @NonNull Throwable throwable) {
                                postValue(new ApiResponse<R>(throwable));
                            }
                        });
                    }
                }
            };
        }
    }

    private static final class BodyCallAdapter<R>
            implements CallAdapter<R, LiveData<R>> {
        private final Type responseType;

        BodyCallAdapter(Type responseType) {
            this.responseType = responseType;
        }

        @Override
        public Type responseType() {
            return responseType;
        }

        @Override
        public LiveData<R> adapt(@NonNull final Call<R> call) {
            return new LiveData<R>() {

                private AtomicBoolean mComputing = new AtomicBoolean(false);

                @Override
                protected void onActive() {
                    if (mComputing.compareAndSet(false, true)) {
                        call.enqueue(new Callback<R>() {
                            @Override
                            public void onResponse(@NonNull Call<R> call, @NonNull Response<R> response) {
                                postValue(response.body());
                            }

                            @Override
                            public void onFailure(@NonNull Call<R> call, @NonNull Throwable throwable) {
                                postValue(null);
                            }
                        });
                    }
                }
            };
        }
    }
    */

    /*
    private abstract static class LiveCallAdapter<R, T> implements CallAdapter<R, LiveCall<R, T>> {
        private final Type responseType;

        LiveCallAdapter(Type responseType) {
            this.responseType = responseType;
        }

        @Override
        public Type responseType() {
            return responseType;
        }

        @Override
        public abstract LiveCall<R, T> adapt(@NonNull Call<R> call);
    }
    */

    private abstract static class BaseCallAdapter<R, T> implements CallAdapter<R, LiveData<T>> {
        private final Type responseType;

        BaseCallAdapter(Type responseType) {
            this.responseType = responseType;
        }

        @Override
        public Type responseType() {
            return responseType;
        }

        @Override
        public abstract LiveData<T> adapt(@NonNull Call<R> call);
    }

    private static final class BodyCallAdapter<R> extends BaseCallAdapter<R, R> {
        BodyCallAdapter(Type responseType) {
            super(responseType);
        }

        @Override
        public LiveData<R> adapt(@NonNull Call<R> call) {
            return new LiveCall<R, R>(call) {
                @Override
                void postResponse(@NonNull Call<R> call, @NonNull Response<R> response) {
                    postValue(response.body());
                }

                @Override
                void postFailure(@NonNull Call<R> call, @NonNull Throwable t) {
                    postValue(null);
                }
            };
        }
    }
/*
    private static final class ApiResponseCallAdapter<R extends ApiBody> extends BaseCallAdapter<R, ApiResponse<R>> {
        ApiResponseCallAdapter(Type responseType) {
            super(responseType);
        }

        @Override
        public LiveData<ApiResponse<R>> adapt(@NonNull Call<R> call) {
            return new LiveCall<R, ApiResponse<R>>(call) {
                @Override
                void postResponse(@NonNull Call<R> call, @NonNull final Response<R> response) {
                    postValue(new ApiResponse<>(response));
                }

                @Override
                void postFailure(@NonNull Call<R> call, @NonNull final Throwable t) {
                    postValue(new ApiResponse<R>(t));
                }
            };
        }
    }
*/
    private static final class ResponseCallAdapter<R> extends BaseCallAdapter<R, Response<R>> {
        ResponseCallAdapter(Type responseType) {
            super(responseType);
        }

        @Override
        public LiveData<Response<R>> adapt(@NonNull Call<R> call) {
            return new LiveCall<R, Response<R>>(call) {
                @Override
                void postResponse(@NonNull Call<R> call, @NonNull Response<R> response) {
                    postValue(response);
                }

                @SuppressWarnings("unchecked")
                @Override
                void postFailure(@NonNull Call<R> call, @NonNull Throwable t) {
                    String content = t.getMessage() != null ? t.getMessage() : t.toString();
                    postValue((Response<R>) Response.error(500,
                            ResponseBody.create(MediaType.parse("text/plain"), content)));
                }
            };
        }
    }
}
