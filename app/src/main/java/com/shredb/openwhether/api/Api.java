package com.shredb.openwhether.api;

import android.arch.lifecycle.LiveData;

import com.shredb.openwhether.model.CityForecast;
import com.shredb.openwhether.data.Forecast;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * REST API access points
 */
public interface Api {
    @GET("data/2.5/find")
    LiveData<Response<ApiBody<CityForecast>>> findCity(@Query("q") String query);

    @GET("data/2.5/forecast")
    LiveData<Response<ApiBody<Forecast>>> getForecasts(@Query("id") long cityId);
}
