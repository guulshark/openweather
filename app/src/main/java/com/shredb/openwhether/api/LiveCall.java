package com.shredb.openwhether.api;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.concurrent.atomic.AtomicBoolean;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A LiveData class that can be invalidated & enqueue a {@link Call}.
 *
 * @param <T> The type of the live data and {@link Call}
 */

public abstract class LiveCall<R, T> extends LiveData<T> implements Callback<R> {
    private Call<R> mCall;
    private AtomicBoolean mComputing = new AtomicBoolean(false);
    private AtomicBoolean mInvalid = new AtomicBoolean(true);

    LiveCall(@NonNull Call<R> call) {
        mCall = call;
    }

    @Override
    protected void onActive() {
        if (mInvalid.get() && mComputing.compareAndSet(false, true)) {
            if (mCall.isCanceled()) {
                mCall = mCall.clone();
            }
            mCall.enqueue(this);
        }
    }

    @Override
    protected void onInactive() {
        if (mInvalid.get() && mComputing.compareAndSet(true, false)) {
            mCall.cancel();
        }
    }

    @Override
    public void onResponse(@NonNull Call<R> call, @NonNull Response<R> response) {
        mInvalid.set(false);
        postResponse(call, response);
        mComputing.set(false);
    }

    @Override
    public void onFailure(@NonNull Call<R> call, @NonNull Throwable t) {
        mInvalid.set(false);
        postFailure(call, t);
        mComputing.set(false);
    }

    /**
     * Invalidates the LiveData.
     * <p>
     * When there are active observers, this will enqueue a {@link Call}.
     */
    public void invalidate() {
        if (mInvalid.compareAndSet(false, true)) {
            if (mComputing.get()) {
                mCall.cancel();
                mCall = mCall.clone();
            } else {
                mComputing.set(true);
            }
            mCall.enqueue(this);
        }
    }

    abstract void postResponse(@NonNull Call<R> call, @NonNull Response<R> response);

    abstract void postFailure(@NonNull Call<R> call, @NonNull Throwable t);
}
