package com.shredb.openwhether.api;

import java.util.List;

public class ApiBody<T> {
    public String cod;
    public String message;
    public int cnt;
    public List<T> list;
}
