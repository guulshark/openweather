package com.shredb.openwhether.api;

import android.support.annotation.NonNull;
import android.util.Log;

import java.io.IOException;
import java.util.Locale;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Add default headers and parameters to all requests to StampnGo api.
 */
public class AddDefaultsInterceptor implements Interceptor {
    private static final String API_KEY = "5cbfddbf12faba8a97350db076c46bde";

    public AddDefaultsInterceptor() {
        super();
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request originRequest = chain.request();
        HttpUrl originalHttpUrl = originRequest.url();

        HttpUrl url = originalHttpUrl.newBuilder()
                .addQueryParameter("units", "metric")
                .addQueryParameter("lang", Locale.getDefault().getLanguage())
                .addQueryParameter("appid", API_KEY)
                .build();

        Request.Builder requestBuilder = originRequest.newBuilder().url(url);
        return chain.proceed(requestBuilder.build());
    }
}
