package com.shredb.openwhether.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import com.shredb.openwhether.R
import com.shredb.openwhether.data.City
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_home.*
import javax.inject.Inject
import android.content.Intent
import android.content.SharedPreferences
import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.shredb.openwhether.util.PrefUtils
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader


class HomeActivity : DaggerAppCompatActivity(), SharedPreferences.OnSharedPreferenceChangeListener {
    private lateinit var adapter: ForecastPagerAdapter
    private lateinit var homeViewModel: HomeViewModel
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { _ ->
            val intent = Intent(this, SelectCityActivity::class.java)
            startActivity(intent)
        }

        setupViewPager()
        setupViewModel()
        readJson()
    }

    override fun onResume() {
        super.onResume()
        PrefUtils.getPreferences(this).registerOnSharedPreferenceChangeListener(this)
        homeViewModel.setSelectedIds(PrefUtils.getSelectedIds(this))
    }

    override fun onPause() {
        super.onPause()
        PrefUtils.getPreferences(this).unregisterOnSharedPreferenceChangeListener(this)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        when (key) {
            PrefUtils.PREF_SELECTED_IDS -> {
                homeViewModel.setSelectedIds(PrefUtils.getSelectedIds(this))
            }
        }
    }

    private fun setupViewPager() {
        tabLayout.setupWithViewPager(viewPager, true)
        adapter = ForecastPagerAdapter(supportFragmentManager)
        viewPager.adapter = adapter
        viewPager.pageMargin = resources.getDimensionPixelSize(R.dimen.page_margin)
    }

    private fun setupViewModel() {
        homeViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(HomeViewModel::class.java)
        homeViewModel.liveSelectedCities.observe(this, Observer<List<City>> { selected ->
            adapter.setCities(selected)
        })
    }

    private fun readJson() {
        var inputStream: InputStream? = null
        var cities: List<City>? = null
        try {
            inputStream = assets.open("city_list.json")
            val reader = BufferedReader(InputStreamReader(inputStream!!))
            cities = Gson().fromJson<List<City>>(reader, object : TypeToken<List<City>>() {}.type)
            reader.close()
        } catch (e: IOException) {
            e.printStackTrace()
            if (inputStream != null) {
                try {
                    inputStream.close()
                } catch (ignore: IOException) {
                    ignore.printStackTrace()
                }

            }
        }


        Log.d("HomeActivity", "onCreate: cities=" + cities!!)
    }
}
