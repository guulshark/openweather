package com.shredb.openwhether.ui;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.shredb.openwhether.data.City;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class ForecastPagerAdapter extends FixedFragmentStatePagerAdapter {
    private static final String TAG = "ForecastPagerAdapter";
    private final List<City> mCities = new ArrayList<>();

    public ForecastPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return ForecastFragment.Companion.getInstance(position, getItemId(position));
    }

    @Override
    public long getItemId(int position) {
        return mCities.get(position).id;
    }

    @Override
    public int getCount() {
        return mCities.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position >= 0 && position < mCities.size()) {
            return mCities.get(position).name;
        } else {
            return super.getPageTitle(position);
        }
    }

    @Override
    public int getItemPosition(Object object) {
        ForecastFragment forecastFragment = (ForecastFragment) object;
        long cityId = forecastFragment.getCityId();
        Log.d(TAG, "getItemPosition: city id = " + cityId);
        if (cityId < 0) {
            Log.d(TAG, "getItemPosition: return " + POSITION_NONE);
            return POSITION_NONE;
        }

        int index = -1;
        for (int i = 0; i < mCities.size(); i++) {
            if (cityId == mCities.get(i).id) {
                index = i;
                break;
            }
        }
        Log.d(TAG, "getItemPosition: index of city = " + index);
        if (index == -1) {
            Log.d(TAG, "getItemPosition: return " + POSITION_NONE);
            return POSITION_NONE;
        } else {
            int position = forecastFragment.getPosition();
            Log.d(TAG, "getItemPosition: position = " + position);
            forecastFragment.setCityId(cityId);
            if (index == position) {
                Log.d(TAG, "getItemPosition: return " + POSITION_UNCHANGED);
                return POSITION_UNCHANGED;
            } else {
                forecastFragment.setPosition(index);
                Log.d(TAG, "getItemPosition: return " + index);
                return index;
            }
        }
    }

    void setCities(@Nullable List<City> cities) {
        Log.d(TAG, "setCities: cities = " + cities);
        mCities.clear();
        if (cities != null) {
            mCities.addAll(cities);
        }
        notifyDataSetChanged();
    }
}
