package com.shredb.openwhether.ui

import android.app.SearchManager
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.ComponentName
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import com.shredb.openwhether.R
import com.shredb.openwhether.data.City
import com.shredb.openwhether.util.PrefUtils
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_select_city.*
import java.util.HashSet
import javax.inject.Inject

class SelectCityActivity : DaggerAppCompatActivity(), SharedPreferences.OnSharedPreferenceChangeListener {
    private val selectedIds = HashSet<Long>()
    private lateinit var adapter: CityAdapter
    private lateinit var cityViewModel: CityViewModel
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_city)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        for (id in PrefUtils.getSelectedIds(this)) {
            selectedIds.add(id)
        }
        setupViewModel()
        setupRecyclerView()
    }

    override fun onResume() {
        super.onResume()
        PrefUtils.getPreferences(this).registerOnSharedPreferenceChangeListener(this)
        adapter.setSelectedIds(PrefUtils.getSelectedIds(this))
    }

    override fun onPause() {
        super.onPause()
        PrefUtils.getPreferences(this).unregisterOnSharedPreferenceChangeListener(this)
        if (isFinishing) {
            PrefUtils.putSelectedIds(this, selectedIds)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu.findItem(R.id.action_search)?.actionView as SearchView
        val componentName = ComponentName(this, SearchCityActivity::class.java)
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setIconifiedByDefault(false)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                NavUtils.navigateUpFromSameTask(this)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        when (key) {
            PrefUtils.PREF_SELECTED_IDS -> {
                adapter.setSelectedIds(PrefUtils.getSelectedIds(this))
            }
        }
    }

    private fun setupRecyclerView() {
        adapter = CityAdapter(selectedIds)
        adapter.setHasStableIds(true)

        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL

        recyclerView.setEmptyView(progress)
        recyclerView.layoutManager = llm
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = adapter
    }

    private fun setupViewModel() {
        cityViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(CityViewModel::class.java)
        cityViewModel.liveCities.observe(this, Observer<List<City>> { items ->
            adapter.setItems(items)
        })
    }
}
