package com.shredb.openwhether.ui;

import android.content.Context;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.util.SortedListAdapterCallback;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shredb.openwhether.R;
import com.shredb.openwhether.data.Forecast;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder> {
    private final SortedList<Forecast> mItems;

    public ForecastAdapter() {
        mItems = new SortedList<>(Forecast.class, new SortedListAdapterCallback<Forecast>(this) {
            @Override
            public int compare(Forecast o1, Forecast o2) {
                return (int) (o1.dt - o2.dt);
            }

            @Override
            public boolean areContentsTheSame(Forecast oldItem, Forecast newItem) {
                return oldItem.dt == newItem.dt
                        && oldItem.main.tempDay == newItem.main.tempDay
                        && oldItem.main.tempNight == newItem.main.tempNight
                        && (oldItem.primaryWeather != null ?
                                oldItem.primaryWeather.equals(newItem.primaryWeather) :
                                newItem.primaryWeather == null);
            }

            @Override
            public boolean areItemsTheSame(Forecast item1, Forecast item2) {
                return item1.cityId == item2.cityId && item1.dt == item2.dt;
            }
        });
    }

    @Override
    public ForecastViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ForecastViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_forecast, parent, false));
    }

    @Override
    public void onBindViewHolder(ForecastViewHolder holder, int position) {
        holder.bind(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void setItems(List<Forecast> items) {
        Log.d("ForecastAdapter", "setItems: items = " + items);
        if (items == null || items.isEmpty()) {
            mItems.clear();
        } else {
            mItems.beginBatchedUpdates();
            try {
                items = new ArrayList<>(items);
                int i = 0;
                while (i < mItems.size()) {
                    int index = items.indexOf(mItems.get(i));
                    if (index != -1) {
                        items.remove(index);
                        i++;
                    } else {
                        mItems.removeItemAt(i);
                    }
                }
                mItems.addAll(items);
            } finally {
                mItems.endBatchedUpdates();
            }
        }
    }

    static class ForecastViewHolder extends RecyclerView.ViewHolder {
        private static final String ICON_PATH_FORMAT = "http://openweathermap.org/img/w/%s.png";

        final ImageView icon;
        final TextView date;
        final TextView description;
        final TextView tempDay;
        final TextView tempNight;

        ForecastViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            description = itemView.findViewById(R.id.description);
            tempNight = itemView.findViewById(R.id.temp_night);
            tempDay = itemView.findViewById(R.id.temp_day);
            icon = itemView.findViewById(R.id.icon);
        }

        void bind(Forecast forecast) {
            final Context context = itemView.getContext();
            date.setText(DateUtils.formatDateTime(context, forecast.dt,
                    DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NO_YEAR
                    | DateUtils.FORMAT_SHOW_WEEKDAY | DateUtils.FORMAT_ABBREV_MONTH));
            if (forecast.primaryWeather != null) {
                description.setText(forecast.primaryWeather.description);
                Picasso.get()
                        .load(String.format(ICON_PATH_FORMAT, forecast.primaryWeather.icon))
                        .resizeDimen(R.dimen.image_size, R.dimen.image_size)
                        .into(icon);
            } else {
                description.setText(null);
                icon.setImageResource(0);
            }
            if (forecast.main.tempDay != Float.MIN_VALUE) {
                tempDay.setText(itemView.getResources().getString(R.string.temperature_unit_cel,
                        Math.round(forecast.main.tempDay)));
            } else {
                tempDay.setText(null);
            }
            if (forecast.main.tempNight != Float.MIN_VALUE) {
                tempNight.setText(itemView.getResources().getString(R.string.temperature_unit_cel,
                        Math.round(forecast.main.tempNight)));
            } else {
                tempNight.setText(null);
            }
        }
    }
/*
    private static class HeaderViewHolder extends RecyclerView.ViewHolder {
        final TextView label;

        HeaderViewHolder(View itemView) {
            super(itemView);
            label = itemView.findViewById(R.id.label);
        }
    }
*/
}
