package com.shredb.openwhether.ui;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.shredb.openwhether.data.City;
import com.shredb.openwhether.model.CityForecast;
import com.shredb.openwhether.data.ForecastRepository;
import com.shredb.openwhether.model.Resource;
import com.shredb.openwhether.util.LiveDataUtils;
import com.shredb.openwhether.util.NullLiveData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

public class CityViewModel extends ViewModel {
    private final LiveData<List<City>> mLiveCities;
    private final LiveData<Resource<List<CityForecast>>> mLiveFoundCities;
    private final MutableLiveData<String> mLiveQuery;
    private final ForecastRepository mRepository;

    @Inject
    public CityViewModel(ForecastRepository repository) {
        mRepository = repository;
        mLiveCities = LiveDataUtils.getDistinctList(mRepository.loadCities());
        mLiveQuery = new MutableLiveData<>();
        mLiveFoundCities = Transformations.switchMap(mLiveQuery, new Function<String, LiveData<Resource<List<CityForecast>>>>() {
            @Override
            public LiveData<Resource<List<CityForecast>>> apply(String query) {
                if (query == null || query.length() == 0) {
                    return NullLiveData.create();
                } else
                    return mRepository.findCity(query);
            }
        });
    }

    public LiveData<List<City>> getLiveCities() {
        return mLiveCities;
    }

    public LiveData<Resource<List<CityForecast>>> getLiveFoundCities() {
        return mLiveFoundCities;
    }

    public LiveData<String> getQuery() {
        return mLiveQuery;
    }

    public void setQuery(String query) {
        String oldQuery = mLiveQuery.getValue();
        Log.d("CityViewModel","setQuery: query = " + query + ", oldQuery = " + oldQuery);
        if (oldQuery == null || !oldQuery.equals(query)) {
            mLiveQuery.setValue(query);
        }
    }

    public void addCities(Collection<CityForecast> cities) {
        if (cities != null && !cities.isEmpty()) {
            List<City> list = new ArrayList<>();
            list.addAll(cities);
            mRepository.addCities(list);
        }
    }
}
