package com.shredb.openwhether.ui;

import android.support.annotation.NonNull;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.util.SortedListAdapterCallback;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.shredb.openwhether.R;
import com.shredb.openwhether.data.City;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;


public class CityAdapter extends RecyclerView.Adapter<CityAdapter.CityViewHolder> {
    private static final Object PAYLOAD_CHECK_CHANGED = new Object();

    private final SortedList<City> mItems;
    private final Set<Long> mSelectedItemIds;
    private final OnSelectedCitiesChangedListener mListener;

    interface OnSelectedCitiesChangedListener {
        void onCityCheckChanged(City city, boolean isChecked);
    }

    public CityAdapter(@NonNull Set<Long> selectedIds) {
        mSelectedItemIds = selectedIds;
        mListener = new OnSelectedCitiesChangedListener() {
            @Override
            public void onCityCheckChanged(City city, boolean isChecked) {
                if (isChecked) {
                    mSelectedItemIds.add(city.id);
                } else {
                    mSelectedItemIds.remove(city.id);
                }
            }
        };
        mItems = new SortedList<>(City.class, new SortedListAdapterCallback<City>(this) {
            @Override
            public int compare(City o1, City o2) {
                return o1.name.compareTo(o2.name);
            }

            @Override
            public boolean areContentsTheSame(City oldItem, City newItem) {
                return oldItem.id == newItem.id
                        && (oldItem.name != null ? oldItem.name.equals(newItem.name) : newItem.name == null)
                        && (oldItem.country != null ? oldItem.country.equals(newItem.country) : newItem.country == null);
            }

            @Override
            public boolean areItemsTheSame(City item1, City item2) {
                return item1.id == item2.id;
            }
        });
    }

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CityViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_city, parent, false), mListener);
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, int position) {
        City city = mItems.get(position);
        holder.bind(city, mSelectedItemIds.contains(city.id));
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, int position, List<Object> payloads) {
        if (payloads.contains(PAYLOAD_CHECK_CHANGED)) {
            City city = mItems.get(position);
            holder.setChecked(mSelectedItemIds.contains(city.id));
            return;
        }
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public long getItemId(int position) {
        return mItems.get(position).id;
    }

    public void setItems(List<City> items) {
        if (items == null || items.isEmpty()) {
            mItems.clear();
        } else {
            mItems.beginBatchedUpdates();
            try {
                items = new ArrayList<>(items);
                int i = 0;
                while (i < mItems.size()) {
                    int index = items.indexOf(mItems.get(i));
                    if (index != -1) {
                        items.remove(index);
                        i++;
                    } else {
                        mItems.removeItemAt(i);
                    }
                }
                mItems.addAll(items);
            } finally {
                mItems.endBatchedUpdates();
            }
        }
    }

    public void setSelectedIds(long[] ids) {
        boolean changed = false;
        for (long id : ids) {
            changed |= mSelectedItemIds.add(id);
        }
        Log.d("CityAdapter", "setSelectedIds: ids = " + Arrays.toString(ids)
                + ", changed = " + changed);
        if (changed) {
            notifyItemRangeChanged(0, getItemCount(), PAYLOAD_CHECK_CHANGED);
        }
    }

    static class CityViewHolder extends RecyclerView.ViewHolder implements
            CompoundButton.OnCheckedChangeListener {

        private final OnSelectedCitiesChangedListener mListener;
        private City mCity;

        final TextView name;
        final TextView country;
        final CheckBox checkbox;

        CityViewHolder(View itemView, OnSelectedCitiesChangedListener listener) {
            super(itemView);
            mListener = listener;
            name = itemView.findViewById(R.id.name);
            country = itemView.findViewById(R.id.country);
            checkbox = itemView.findViewById(R.id.checkbox);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkbox.performClick();
                }
            });
        }

        void bind(City city, boolean checked) {
            mCity = city;
            name.setText(city.name);
            if (city.country != null && !city.country.isEmpty()) {
                Locale locale = new Locale(Locale.getDefault().getLanguage(), city.country);
                country.setText(locale.getDisplayCountry());
            } else {
                country.setText(null);
            }
            name.setContentDescription(city.name);
            setChecked(checked);
        }

        final void setChecked(boolean checked) {
            // avoid dispatching changes when recycling views
            checkbox.setOnCheckedChangeListener(null);
            checkbox.setChecked(checked);
            checkbox.setOnCheckedChangeListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            mListener.onCityCheckChanged(mCity, isChecked);
        }
    }
}
