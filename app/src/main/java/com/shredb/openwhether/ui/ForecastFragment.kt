package com.shredb.openwhether.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shredb.openwhether.R
import com.shredb.openwhether.data.Forecast
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_forecast.*
import javax.inject.Inject

class ForecastFragment : DaggerFragment() {
    private lateinit var adapter: ForecastAdapter
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    companion object {

        fun getInstance(position: Int, cityId: Long): ForecastFragment {
            val fragment = ForecastFragment()
            val args = Bundle()
            args.putInt("position", position)
            args.putLong("city_id", cityId)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_forecast, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        setupRecyclerView()
        setupViewModel()
    }

    private fun setupRecyclerView() {
        adapter = ForecastAdapter()

        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL

        recyclerView.layoutManager = llm
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = adapter
        recyclerView.setEmptyView(progress)
    }

    private fun setupViewModel() {
        val forecastViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(ForecastViewModel::class.java)
        forecastViewModel.liveForecasts.observe(this, Observer<List<Forecast>> { items ->
            adapter.setItems(items)
        })
        forecastViewModel.setCityId(getCityId())
    }

    fun getCityId(): Long = arguments.getLong("city_id")

    fun getPosition(): Int = arguments.getInt("position")

    fun setCityId(cityId: Long) {
        arguments.putLong("city_id", cityId)
        ViewModelProviders.of(this, viewModelFactory).get(ForecastViewModel::class.java)
                .setCityId(cityId)
    }

    fun setPosition(position: Int) {
        arguments.putInt("position", position)
    }
}
