package com.shredb.openwhether.ui;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.shredb.openwhether.data.Forecast;
import com.shredb.openwhether.data.ForecastRepository;
import com.shredb.openwhether.util.LiveDataUtils;
import com.shredb.openwhether.util.NullLiveData;

import java.util.List;

import javax.inject.Inject;

public class ForecastViewModel extends ViewModel {
    private final MutableLiveData<Long> mLiveCityId;
    private final LiveData<List<Forecast>> mLiveForecasts;
    private final ForecastRepository mRepository;

    @Inject
    public ForecastViewModel(ForecastRepository repository) {
        mRepository = repository;
        mLiveCityId = new MutableLiveData<>();
        mLiveForecasts = LiveDataUtils.getDistinctList(Transformations.switchMap(mLiveCityId,
                new Function<Long, LiveData<List<Forecast>>>() {
                    @Override
                    public LiveData<List<Forecast>> apply(Long cityId) {
                        if (cityId == null) {
                            return NullLiveData.create();
                        } else {
                            return mRepository.loadForecasts(cityId);
                        }
                    }
                }));
    }


    public void setCityId(long cityId) {
        Long curCityId = mLiveCityId.getValue();
        if (curCityId == null || !curCityId.equals(cityId)) {
            mLiveCityId.setValue(cityId);
        }
    }

    public LiveData<List<Forecast>> getLiveForecasts() {
        return mLiveForecasts;
    }
}
