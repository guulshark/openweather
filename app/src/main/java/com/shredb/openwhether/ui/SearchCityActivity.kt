package com.shredb.openwhether.ui

import android.app.SearchManager
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.text.InputType
import android.text.SpannableString
import android.text.Spanned
import android.text.TextUtils
import android.text.style.StyleSpan
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import com.shredb.openwhether.R
import com.shredb.openwhether.model.CityForecast
import com.shredb.openwhether.model.Resource
import com.shredb.openwhether.util.PrefUtils
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_search_city.*
import java.util.*
import javax.inject.Inject


class SearchCityActivity : DaggerAppCompatActivity() {
    private lateinit var adapter: CityForecastAdapter
    private lateinit var cityViewModel: CityViewModel
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_city)

        setupViewModel()
        setupRecyclerView()
        setupSearchView()
        handleIntent(intent)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        handleIntent(intent)
    }

    override fun onPause() {
        super.onPause()
        if (isFinishing && !adapter.selectedItems.isEmpty()) {
            cityViewModel.addCities(adapter.selectedItems)
            val ids = HashSet<Long>()
            for (city in adapter.selectedItems) {
                ids.add(city.id)
            }
            val selectedIds = PrefUtils.getSelectedIds(this)
            Log.d("Search","selectedIds = " + Arrays.toString(selectedIds))
            for (id in selectedIds) {
                ids.add(id)
            }
            Log.d("Search", "ids = $ids")
            PrefUtils.putSelectedIds(this, ids)
        }
    }

    private fun handleIntent(intent: Intent?) {
        if (intent != null && intent.hasExtra(SearchManager.QUERY)) {
            val query = intent.getStringExtra(SearchManager.QUERY)
            if (!TextUtils.isEmpty(query)) {
                Log.d("Search", "handleIntent: query = $query")
                searchView.setQuery(query, true)
            }
        }
    }

    private fun setupRecyclerView() {
        adapter = CityForecastAdapter()
        adapter.setHasStableIds(true)

        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL

        recyclerView.layoutManager = llm
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = adapter
    }

    private fun setupViewModel() {
        cityViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(CityViewModel::class.java)
        cityViewModel.liveFoundCities.observe(this, Observer<Resource<List<CityForecast>>> { resource ->
            Log.d("Search", "onChange(resource): ")
            if (resource == null || resource.status == Resource.Status.ERROR
                    || resource.status == Resource.Status.SUCCESS && (resource.data == null || resource.data.isEmpty())) {
                Log.d("Search", "onChange(resource): error or empty")
                adapter.setItems(null)
                progress.visibility = View.GONE
                recyclerView.visibility = View.GONE
                result.visibility = View.VISIBLE

                val lastQuery = cityViewModel.query.value
                if (lastQuery != null && lastQuery.isNotEmpty()) {
                    val format = getString(R.string.empty_query)
                    val start = format.indexOf("%s")
                    val end = start + lastQuery.length
                    val text = String.format(format, lastQuery)
                    if (start in 0..(end - 1)) {
                        val spanText = SpannableString(text);
                        spanText.setSpan(StyleSpan (Typeface.BOLD), start, end,
                                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                        result.text = spanText
                    } else {
                        result.text = text
                    }
                } else {
                    result.text = getString(R.string.title_search_city)
                }
            } else {
                if (resource.status == Resource.Status.LOADING) {
                    Log.d("Search", "onChange(resource): loading")
                    recyclerView.visibility = View.GONE
                    result.visibility = View.GONE
                    progress.visibility = View.VISIBLE
                } else {
                    Log.d("Search", "onChange(resource): success")
                    adapter.setItems(resource.data)
                    progress.visibility = View.GONE
                    result.visibility = View.GONE
                    recyclerView.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun setupSearchView() {
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.inputType = InputType.TYPE_TEXT_FLAG_CAP_WORDS
        searchView.imeOptions = searchView.imeOptions or EditorInfo.IME_ACTION_SEARCH or
                EditorInfo.IME_FLAG_NO_EXTRACT_UI or EditorInfo.IME_FLAG_NO_FULLSCREEN
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                if (TextUtils.isEmpty(newText)) {
                    cityViewModel.setQuery(null)
                    return true
                }
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                Log.d("Search", "onQueryTextSubmit: query = $query")
                cityViewModel.setQuery(query)
                return true
            }
        })
        searchBack.setOnClickListener({
            if (!isFinishing) {
                //onBackPressed()
                NavUtils.navigateUpFromSameTask(this)
            }
        })
    }
}
