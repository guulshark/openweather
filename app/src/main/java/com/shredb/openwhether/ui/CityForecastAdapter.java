package com.shredb.openwhether.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.util.SortedListAdapterCallback;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.shredb.openwhether.R;
import com.shredb.openwhether.data.City;
import com.shredb.openwhether.model.CityForecast;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;


public class CityForecastAdapter extends RecyclerView.Adapter<CityForecastAdapter.CityViewHolder> {
    private static final String ICON_PATH_FORMAT = "http://openweathermap.org/img/w/%s.png";

    private final SortedList<CityForecast> mItems;
    private final Set<CityForecast> mSelectedItems;
    private final OnSelectedCitiesChangedListener mListener;

    interface OnSelectedCitiesChangedListener {
        void onCityCheckChanged(CityForecast city, boolean isChecked);
    }

    public CityForecastAdapter() {
        mSelectedItems = new HashSet<>();
        mListener = new OnSelectedCitiesChangedListener() {
            @Override
            public void onCityCheckChanged(CityForecast city, boolean isChecked) {
                if (isChecked) {
                    mSelectedItems.add(city);
                } else {
                    mSelectedItems.remove(city);
                }
            }
        };
        mItems = new SortedList<>(CityForecast.class, new SortedListAdapterCallback<CityForecast>(this) {
            @Override
            public int compare(CityForecast o1, CityForecast o2) {
                return o1.name.compareTo(o2.name);
            }

            @Override
            public boolean areContentsTheSame(CityForecast oldItem, CityForecast newItem) {
                return oldItem.id == newItem.id
                        && (oldItem.name != null ? oldItem.name.equals(newItem.name) : newItem.name == null)
                        && (oldItem.country != null ? oldItem.country.equals(newItem.country) : newItem.country == null)
                        && oldItem.main.temp == newItem.main.temp
                        && (oldItem.weather != null ? oldItem.weather.equals(newItem.weather) : newItem.weather == null);
            }

            @Override
            public boolean areItemsTheSame(CityForecast item1, CityForecast item2) {
                return item1.id == item2.id;
            }
        });
    }

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CityViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_city_forecast, parent, false), mListener);
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, int position) {
        CityForecast city = mItems.get(position);
        holder.bind(city, mSelectedItems.contains(city));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public long getItemId(int position) {
        return mItems.get(position).id;
    }

    public void setItems(List<CityForecast> items) {
        if (items == null || items.isEmpty()) {
            mItems.clear();
        } else {
            mItems.beginBatchedUpdates();
            try {
                items = new ArrayList<>(items);
                int i = 0;
                while (i < mItems.size()) {
                    int index = items.indexOf(mItems.get(i));
                    if (index != -1) {
                        items.remove(index);
                        i++;
                    } else {
                        mItems.removeItemAt(i);
                    }
                }
                mItems.addAll(items);
            } finally {
                mItems.endBatchedUpdates();
            }
        }
    }

    public Set<CityForecast> getSelectedItems() {
        return mSelectedItems;
    }

    static class CityViewHolder extends RecyclerView.ViewHolder implements
            CompoundButton.OnCheckedChangeListener {

        private final OnSelectedCitiesChangedListener mListener;
        private CityForecast mCity;

        final TextView name;
        final CheckBox checkbox;
        final ImageView icon;
        final TextView date;
        final TextView description;
        final TextView temp;

        CityViewHolder(View itemView, OnSelectedCitiesChangedListener listener) {
            super(itemView);
            mListener = listener;
            name = itemView.findViewById(R.id.name);
            checkbox = itemView.findViewById(R.id.checkbox);
            date = itemView.findViewById(R.id.date);
            description = itemView.findViewById(R.id.description);
            temp = itemView.findViewById(R.id.temp);
            icon = itemView.findViewById(R.id.icon);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkbox.performClick();
                }
            });
        }

        void bind(CityForecast city, boolean checked) {
            mCity = city;
            StringBuilder nameBuilder = new StringBuilder(city.name);
            if (city.country != null && !city.country.isEmpty()) {
                Locale locale = new Locale(Locale.getDefault().getLanguage(), city.country);
                nameBuilder.append(" (").append(locale.getDisplayCountry()).append(")");
            }
            name.setText(nameBuilder.toString());
            setChecked(checked);

            final Context context = itemView.getContext();
            date.setText(DateUtils.formatDateTime(context, city.dt,
                    DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NO_YEAR
                            | DateUtils.FORMAT_SHOW_WEEKDAY | DateUtils.FORMAT_ABBREV_MONTH));
            if (city.weather != null && city.weather.size() > 0) {
                description.setText(city.weather.get(0).description);
                Picasso.get()
                        .load(String.format(ICON_PATH_FORMAT, city.weather.get(0).icon))
                        .resizeDimen(R.dimen.image_size, R.dimen.image_size)
                        .into(icon);
            } else {
                description.setText(null);
                icon.setImageResource(0);
            }
            temp.setText(itemView.getResources().getString(R.string.temperature_unit_cel,
                    Math.round(city.main.temp)));
        }

        final void setChecked(boolean checked) {
            // avoid dispatching changes when recycling views
            checkbox.setOnCheckedChangeListener(null);
            checkbox.setChecked(checked);
            checkbox.setOnCheckedChangeListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            mListener.onCityCheckChanged(mCity, isChecked);
        }
    }
}
