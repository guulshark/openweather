package com.shredb.openwhether.ui;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.shredb.openwhether.data.City;
import com.shredb.openwhether.data.ForecastRepository;
import com.shredb.openwhether.util.LiveDataUtils;
import com.shredb.openwhether.util.NullLiveData;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

public class HomeViewModel extends ViewModel {
    private final MutableLiveData<long[]> mLiveSelectedIds;
    private final LiveData<List<City>> mLiveSelectedCities;
    private final ForecastRepository mRepository;

    @Inject
    public HomeViewModel(ForecastRepository repository) {
        mRepository = repository;
        mLiveSelectedIds = new MutableLiveData<>();
        mLiveSelectedCities = LiveDataUtils.getDistinctList(Transformations.switchMap(mLiveSelectedIds,
                new Function<long[], LiveData<List<City>>>() {
                    @Override
                    public LiveData<List<City>> apply(long[] input) {
                        if (input == null || input.length == 0) {
                            return NullLiveData.create();
                        } else {
                            return mRepository.loadSelectedCities(input);
                        }
                    }
                }));
    }

    public void setSelectedIds(long[] ids) {
        if (ids != null) Arrays.sort(ids);
        long[] curIds = mLiveSelectedIds.getValue();
        if (!Arrays.equals(curIds, ids)) {
            mLiveSelectedIds.setValue(ids);
        }
    }

    public LiveData<List<City>> getLiveSelectedCities() {
        return mLiveSelectedCities;
    }
}
