package com.shredb.openwhether.model;

import android.support.annotation.IntDef;
import android.support.annotation.Nullable;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * A generic class that holds a value with its loading status.
 * @param <T>
 */
public class Resource<T> {

    /**
     * Status of a resource that is provided to the UI.
     */
    @IntDef({Status.LOADING, Status.SUCCESS, Status.ERROR})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Status {
        int LOADING = 0;
        int SUCCESS = 1;
        int ERROR = 2;
    }

    @Status
    public final int status;
    @Nullable
    public final T data;

    private Resource(@Status int status, @Nullable T data) {
        this.status = status;
        this.data = data;
    }

    public static <T> Resource<T> success(@Nullable T data) {
        return new Resource<>(Status.SUCCESS, data);
    }

    public static <T> Resource<T> error(@Nullable T data) {
        return new Resource<>(Status.ERROR, data);
    }

    public static <T> Resource<T> loading(@Nullable T data) {
        return new Resource<>(Status.LOADING, data);
    }

    @Override
    public String toString() {
        return "Resource{" +
                "status=" + status +
                ", data=" + data +
                '}';
    }
}
