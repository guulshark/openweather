package com.shredb.openwhether.model;

import com.shredb.openwhether.data.City;

import java.util.List;

public class CityForecast extends City {
    public long dt;
    public Main main;
    public List<Weather> weather;
}
