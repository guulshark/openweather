package com.shredb.openwhether.model;

public class Weather {
    public static final Weather EMPTY = new Weather();

    public String description;
    public String icon;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Weather)) return false;

        Weather weather = (Weather) o;

        if (description != null ? !description.equals(weather.description) : weather.description != null)
            return false;
        return icon != null ? icon.equals(weather.icon) : weather.icon == null;
    }

    @Override
    public int hashCode() {
        int result = description != null ? description.hashCode() : 0;
        result = 31 * result + (icon != null ? icon.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Weather{" +
                "description='" + description + '\'' +
                ", icon='" + icon + '\'' +
                '}';
    }
}
