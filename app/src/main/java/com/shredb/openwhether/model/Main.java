package com.shredb.openwhether.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Ignore;

public class Main {
    @Ignore
    public float temp;
    @ColumnInfo(name = "temp_day")
    public float tempDay;
    @ColumnInfo(name = "temp_night  ")
    public float tempNight;

    @Override
    public String toString() {
        return "Main{" +
                "temp=" + temp +
                ", tempDay=" + tempDay +
                ", tempNight=" + tempNight +
                '}';
    }
}
